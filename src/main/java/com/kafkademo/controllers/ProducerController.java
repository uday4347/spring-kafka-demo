package com.kafkademo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
@RequestMapping("/api")
public class ProducerController {

    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    public ProducerController(KafkaTemplate<String, String> kafkaTemplate){
        this.kafkaTemplate = kafkaTemplate;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/produce/{topicName}", consumes = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> produce(@PathVariable String topicName, @RequestBody String message){
        Arrays.stream(message.split(",")).forEach(msg -> sendMessage(topicName, msg));
        return ResponseEntity.ok("Message sent to Kafka!");
    }

    public void sendMessage(String topicName, String msg) {
        System.out.println("Message - " + msg);
        kafkaTemplate.send(topicName, msg);
    }
}
